import { Component, OnInit, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { trigger,state, style, transition, animate, keyframes, query, stagger } from '@angular/animations';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css'],
  animations: [
    trigger('flyInOutV', [
    state('in', style({ transform: 'translateX(0)' })),
    transition('void => *', [
      style({ transform: 'translateX(-100%)' }),
      animate(200)
    ]),
    transition('* => void', [
      animate(200, style({ transform: 'translateX(100%)' }))
    ])
  ]),
  trigger('listAnimation', [
    transition('* => *', [ // each time the binding value changes
      query(':leave', [
        stagger(1000, [
          animate('1s', style({ opacity: 0 }))
        ])
      ], {optional: true}),
      query(':enter', [
        style({ opacity: 0 }),
        stagger(1000, [
          animate('1s', style({ opacity: 1 }))
        ])
      ], {optional: true})
    ])
  ])
  ]
})

export class VideoComponent implements OnInit {
  public ydata: any;
  constructor(private http: HttpClient) {
    this.http.get('https://www.googleapis.com/youtube/v3/search?key=AIzaSyDzYNFwrFl47RIy2LrOIha3LpFa5oDMX2Y&channelId=UCU19wue-Vwq0WqfYLnX4zqA&part=snippet,id&order=date&maxResults=20').subscribe((res)=>{
        this.ydata = res['items'];
        console.log(this.ydata);
    });
  }
  ngOnInit() {
  }

}
