import { Component, OnInit } from '@angular/core';
import { trigger,state, style, transition, animate, keyframes, query, stagger } from '@angular/animations';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css'],
  animations: [
    trigger('flyInOutE', [
    state('in', style({ transform: 'translateX(0)' })),
    transition('void => *', [
      style({ transform: 'translateX(-100%)' }),
      animate(200)
    ]),
    transition('* => void', [
      animate(200, style({ transform: 'translateX(100%)' }))
    ])
  ])
  ]
})
export class EducationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
