import { Component, OnInit } from '@angular/core';
import { trigger,state, style, transition, animate, keyframes, query, stagger } from '@angular/animations';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css'],
  animations: [
    trigger('flyInOutS', [
    state('in', style({ transform: 'translateX(0)' })),
    transition('void => *', [
      style({ transform: 'translateX(-100%)' }),
      animate(200)
    ]),
    transition('* => void', [
      animate(200, style({ transform: 'translateX(100%)' }))
    ])
  ])
  ]
})
export class SkillsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
