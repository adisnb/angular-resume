import { TestBed } from '@angular/core/testing';

import { YtubeservicesService } from './ytubeservices.service';

describe('YtubeservicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: YtubeservicesService = TestBed.get(YtubeservicesService);
    expect(service).toBeTruthy();
  });
});
