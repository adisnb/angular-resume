import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class YtubeservicesService {

  constructor(private http: HttpClient) { }
  configUrl = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyDzYNFwrFl47RIy2LrOIha3LpFa5oDMX2Y&channelId=UCU19wue-Vwq0WqfYLnX4zqA&part=snippet,id&order=date&maxResults=20';

  getConfig() {
    return this.http.get(this.configUrl);
  }
}
