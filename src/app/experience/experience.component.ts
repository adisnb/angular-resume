import { Component, OnInit } from '@angular/core';
import { trigger,state, style, transition, animate, keyframes, query, stagger } from '@angular/animations';
@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.css'],
  animations: [
    trigger('flyInOut', [
    state('in', style({ transform: 'translateX(0)' })),
    transition('void => *', [
      style({ transform: 'translateX(-100%)' }),
      animate(200)
    ]),
    transition('* => void', [
      animate(200, style({ transform: 'translateX(100%)' }))
    ])
  ])
  ]
})

export class ExperienceComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }

}
