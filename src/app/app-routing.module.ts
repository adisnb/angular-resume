import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SkillsComponent }      from './skills/skills.component';
import { EducationComponent }   from './education/education.component';
import { ExperienceComponent }   from './experience/experience.component';
import { VideoComponent }   from './video/video.component';
const routes: Routes = [
  { path: 'skills', component: SkillsComponent },
  { path: 'education', component: EducationComponent },
  { path: 'experience', component: ExperienceComponent },
  { path: 'video', component: VideoComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
